package com.nicknytko.android.banana;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class Accelerometer implements SensorEventListener
{
    private SensorManager sensMan;
    private float x,y,z;

    public Accelerometer()
    {
        sensMan = (SensorManager)Api.getMainActivity().getSystemService(Context.SENSOR_SERVICE);
        x = y = z = 0.0f;
    }

    public void startSensing()
    {
        sensMan.registerListener(this, sensMan.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_GAME );
    }

    public void stopSensing()
    {
        sensMan.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent)
    {
        x = sensorEvent.values[0];
        y = sensorEvent.values[1];
        z = sensorEvent.values[2];
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) { }

    public float getX(){ return x; }
    public float getY(){ return y; }
    public float getZ(){ return z; }
    public float[] getValues(){ return new float[]{x,y,z}; }

    public float getMax(){ return sensMan.GRAVITY_EARTH; }
}
