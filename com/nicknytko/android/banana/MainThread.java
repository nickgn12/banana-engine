package com.nicknytko.android.banana;

import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

public class MainThread extends Thread {
    //controls what actually happens
    public boolean running;
    private SurfaceHolder surfaceHolder;
    private final float FRAME_TIME = (1000.0f / (float) Config.FPS_CAP);
    private Game game;

    public void initialize(SurfaceHolder surfaceHolder) {
        this.surfaceHolder = surfaceHolder;
        game = new Game();
        Api.registerGame(game);

        game.initialize();
    }

    public void deinitialize() {
        game.getGamemode().onDie();
    }

    public boolean onTouch(MotionEvent event) {
        return game.onTouch(event);
    }

    public void onDraw(Canvas c) {
        Api.getGraphicsSubsystem().setCanvas(c);
        game.onDraw(Api.getGraphicsSubsystem());
    }

    @Override
    public void run() {
        Api.getDebug().out("Started thread");

        Canvas c = null;
        long startTime, endTime;

        while (running) {
            try {
                c = surfaceHolder.lockCanvas();
                synchronized (surfaceHolder) {
                    startTime = System.currentTimeMillis();

                    Api.incrementTicks();
                    game.onTick();

                    if (c != null) {
                        Api.getGraphicsSubsystem().setCanvas(c);
                        game.onDraw(Api.getGraphicsSubsystem());
                    }

                    endTime = System.currentTimeMillis();

                    if (Config.showFPS)
                    {
                        Api.getGraphicsSubsystem().setFontColor(255, 255, 255);
                        Api.getGraphicsSubsystem().setFontSize(5);

                        float deltaTime = endTime - startTime;

                        Api.getGraphicsSubsystem().drawText(15, 15, "ms: " + Integer.toString((int)deltaTime) + " (" + Integer.toString((int)((deltaTime / FRAME_TIME) * 100.0f)) + " %)");
                    }

                    if (endTime < startTime + FRAME_TIME) {
                        try {
                            Thread.sleep((long) (FRAME_TIME - (endTime - startTime)));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            } finally {
                if (c != null) {
                    try {
                        surfaceHolder.unlockCanvasAndPost(c);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}