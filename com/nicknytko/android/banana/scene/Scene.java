package com.nicknytko.android.banana.scene;

import com.nicknytko.android.banana.GraphicsSubsystem;

public class Scene {
    private Graph objectGraph;

    public Scene() {
        objectGraph = new Graph();
    }

    public void onTick() {
        objectGraph.sortNodes();
    }

    public void onDraw(GraphicsSubsystem g) {
        objectGraph.onDraw(g);
    }

    public void addNode(BaseNode n) {
        objectGraph.addNode(n);
    }

    public void clearNodes() {
        objectGraph.clearNodes();
    }
}