package com.nicknytko.android.banana.scene;

import com.nicknytko.android.banana.GraphicsSubsystem;

public class BaseNode {
    public BaseNode() {
        x = y = z = 0;
        changed = false;
        dead = false;
    }

    public int x, y, z;
    public boolean changed; //this needs to be set to true in order for your sprite to be resorted
    public boolean dead;

    public void onDraw(GraphicsSubsystem g) {
    }

    public int getDepth() {
        return y;
    }
}