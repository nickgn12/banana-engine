package com.nicknytko.android.banana.particle;

import com.nicknytko.android.banana.Api;
import com.nicknytko.android.banana.GraphicsSubsystem;

public class SquareParticle extends BaseParticle
{
	private int r,g,b;
	private int width, height;
	public long lifespan;
	
	public SquareParticle(int x, int y, int width, int height, float xvel, float yvel, int r, int g, int b) {
		super(x, y, xvel, yvel);
		this.r = r;
		this.g = g;
		this.b = b;
		this.width = width;
		this.height = height;
		
		lifespan = 100;
		// TODO Auto-generated constructor stub
	}
	
	public long getLifespan(){ return lifespan; }

	public void onDraw( GraphicsSubsystem g )
	{
		g.setColor(this.r,this.g,this.b);
		g.drawRectangle(x - width/2 - Api.getCamera().getX(), y - height/2 - Api.getCamera().getY(), width, height);
	}
}