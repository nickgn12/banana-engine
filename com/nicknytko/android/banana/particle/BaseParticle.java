package com.nicknytko.android.banana.particle;

import com.nicknytko.android.banana.GraphicsSubsystem;

public class BaseParticle
{
	public int x,y;
	public float xspeed,yspeed;
	public boolean dead;
	protected long createTime;
	
	public BaseParticle( int x, int y, float xvel, float yvel )
	{
		this.x = x;
		this.y = y;
		xspeed = xvel;
		yspeed = yvel;
		dead = false;
		createTime = System.currentTimeMillis();
	}
	
	public long getLifespan(){ return 1000; }
    public long getTimeLeft(){ return (createTime + getLifespan()) - System.currentTimeMillis(); }
    public float getLifePercentage(){ return ((float)(System.currentTimeMillis() - createTime) / (float)getLifespan()); }
	
	public void onTick()
	{
		x += (int)xspeed;
		y += (int)yspeed;
		
		if (!dead && System.currentTimeMillis() > createTime+getLifespan()){ dead = true; }
	}
	
	public void onDraw( GraphicsSubsystem g )
	{
	}
}