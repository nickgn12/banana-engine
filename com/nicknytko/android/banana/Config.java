package com.nicknytko.android.banana;

import com.nicknytko.android.banana.example.ExampleActivity;
import com.nicknytko.android.banana.example.ExampleGamemode;

public class Config
{
    //Change these as they are app-specific
    public static String appPackageName = "com.example.app";
    public static Class appActivityClass = ExampleActivity.class;
    public static Class appRDrawableClass = R.drawable.class;
    public static Class defaultGamemode = ExampleGamemode.class;

    //If you try to use any of these and they are set to false, your app will crash!
    public static boolean useSceneGraph = false;
    public static boolean useVibrator = true;
    public static boolean useAccelerometer = true;
    public static boolean useMusic = true;
    public static boolean useSound = true;
    public static boolean useNetwork = false;

    public static boolean drawParticlesBeforeEntities = true; //shouldn't matter if you use the scene graph
    public static int FPS_CAP = 60;
    public static boolean showFPS = false;

    public static int joystickImage = R.drawable.joystick;
    public static int joystickTopImage = R.drawable.joytop;
}
