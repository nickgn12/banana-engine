package com.nicknytko.android.banana;

import android.media.SoundPool;

public class Sound
{
	private int soundId;
	private int rawId;
    private int streamId;
    private boolean loaded;

    public boolean muted;

    public Sound( int soundId, int rawId )
    {
        this.soundId = soundId;
        this.rawId = rawId;
        streamId = 0;
        loaded = false;
    }

	public int getId(){ return soundId; }
	public void setId( int id ){ soundId = id; }
	
	public int getRawId(){ return rawId; }
	public void setRawId( int rid ){ rawId = rid; }

    public void setStreamId( int id ){ streamId = id; }
    public int getStreamId( ){ return streamId; }

    public boolean getLoaded(){ return loaded; }
    public boolean isLoaded(){ return loaded; }

    public void setLoaded(){ loaded = true; }
}