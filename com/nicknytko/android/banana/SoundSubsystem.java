package com.nicknytko.android.banana;

import java.util.Vector;

import android.media.AudioManager;
import android.media.SoundPool;

public class SoundSubsystem implements SoundPool.OnLoadCompleteListener
{
	private SoundPool pool;
	private Vector<Sound> sounds;
	
	public SoundSubsystem()
	{
		sounds = new Vector<Sound>();
		pool = new SoundPool(16, AudioManager.STREAM_MUSIC, 100 );
        pool.setOnLoadCompleteListener( this );
	}
	
	private Sound loadSoundFromId( int rid )
	{
		int soundId = pool.load(Api.getMainActivity(), rid, 1);
		Sound ret = new Sound(soundId,rid);
		sounds.add(ret);
		
		return ret;
	}
	
	private int isSoundLoaded( int rid ) //do we have a sound object?
	{
		if (sounds.size() == 0){ return -1; }
		
		for (int i=0;i < sounds.size();i++)
		{
			if (sounds.get(i).getRawId() == rid){ return i; }
		}
		
		return -1;
	}
		
	public Sound loadSound( int rid )
	{
		int soundLoaded = isSoundLoaded(rid);
		
		if (soundLoaded == -1)
		{
            Api.getDebug().out("Loaded new sound");
			return loadSoundFromId(rid);
		}
		else
		{
			return sounds.get(soundLoaded);
		}
	}
	
	public void playSound( Sound s )
	{
        if (s.muted)
            return;

		s.setStreamId( pool.play ( s.getId(), 1.0f, 1.0f, 1, 0, 1.0f ) );
	}
	
	public void loopSound( Sound s )
	{
        if (s.muted)
            return;

		s.setStreamId( pool.play(s.getId(), 1.0f, 1.0f, 1, -1, 1.0f) );
    }
	
	public void stopSound( Sound s )
	{
		pool.stop(s.getStreamId());
	}

    public void pauseAllSounds( )
    {
        pool.autoPause();
    }

    public void resumeAllSounds( )
    {
        pool.autoResume();
    }
	
	public void stopAllSounds( )
	{
		for (int i=0;i < sounds.size();i++)
		{
			pool.stop(sounds.get(i).getStreamId());
		}
	}
	
	public void unloadAllSounds()
	{
        Api.getDebug().out("Unloaded all sounds.");

		for (int i=0;i < sounds.size();++i)
		{
			pool.stop(sounds.get(i).getStreamId());
			pool.unload(sounds.get(i).getId());
		}
		
		sounds.clear();
	}

    @Override
    public void onLoadComplete(SoundPool soundPool, int sampleId, int status)
    {
        if (soundPool != this.pool)
            return;

        if (status != 0)
            return;

        Api.getDebug().out("Finished loading sound " + sampleId);

        for (int i=0;i < sounds.size();++i)
        {
            if (sounds.get(i).getId() == sampleId)
                sounds.get(i).setLoaded();
        }
    }
};