package com.nicknytko.android.banana;

import android.content.Context;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

public class BananaView extends SurfaceView implements SurfaceHolder.Callback {
    private MainThread thread;

    public BananaView(Context context) {
        super(context);
        getHolder().addCallback(this);

        setFocusable(true);
        setDrawingCacheEnabled(true);
        setWillNotDraw(false);
        setKeepScreenOn(true);
    }

    //@Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    //@Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (thread == null) {
            Api.registerGraphicsSubsystem(new GraphicsSubsystem());

            thread = new MainThread();

            thread.initialize(getHolder());
            thread.running = true;
            thread.start();

            Api.registerMainThread(thread);
        }
    }

    //@Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        thread.running = false;
        thread.deinitialize();

        Api.getMusicPlayer().stop();
        Api.getSoundSubsystem().stopAllSounds();

        boolean retry = true;
        while (retry) {
            try {

                thread.join();
                retry = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        thread = null;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (thread == null) {
            return false;
        }
        try {
            thread.onTouch(event);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}