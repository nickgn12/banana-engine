package com.nicknytko.android.banana;

import android.util.Log;

public class Debug {
    public void out(String msg) {
        try {
            if (msg.equals("")) {
                return;
            }
            if (msg.equals(null)) {
                return;
            }
            Log.d(Config.appActivityClass.getSimpleName(), msg);
        } catch (NullPointerException e) {
            Log.d(Config.appActivityClass.getSimpleName(), "Tried to print null pointer");
        }
    }
}