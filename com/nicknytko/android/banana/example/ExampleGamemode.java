package com.nicknytko.android.banana.example;

import com.nicknytko.android.banana.Gamemode;
import com.nicknytko.android.banana.GraphicsSubsystem;

public class ExampleGamemode extends Gamemode
{
    public void initialize()
    {
    }

    public void onDraw(GraphicsSubsystem g)
    {
        g.clearScreen(255,255,255); //Set the screen to white

        g.setFontSize(50); //50 point font
        g.setFontColor(0,0,0); //black font
        g.drawCenterText(g.getHalfWidth(),g.getHalfHeight(),"Hello, World!"); //display "Hello, World!" in the middle of the screen
    }

    public void onTick()
    {
    }
}