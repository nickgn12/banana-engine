package com.nicknytko.android.banana.example;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;

import com.nicknytko.android.banana.Api;

public class ExampleActivity extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Api.registerMainActivity(this);
        Api.initEngine();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Api.getSoundSubsystem().resumeAllSounds();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        Api.getSoundSubsystem().stopAllSounds();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                Api.simulateBackButton();
                return true;
            default:
                Api.simulateButtonPress(keyCode);
                return true;
        }
    }

    @Override
    public void onBackPressed() {
        Api.simulateBackButton();
    }
}
