package com.nicknytko.android.banana;

import com.nicknytko.android.banana.particle.BaseParticle;

import android.view.MotionEvent;
import android.widget.Toast;

public class Game {
    private Gamemode currentMode;
    private Gamemode changeGamemodeRequest;
    private Joystick js;

    private boolean paused;

    private Entity[] entityArray;
    private int entCount;
    private Projectile[] projectileArray;
    private int proCount; //projectile count
    private BaseParticle[] particleArray;
    private int particleCount;
    
    public Level level;

    public void handleBackButton() {
        if (currentMode != null) {
            currentMode.onBackButton();
        }
    }

    public void initialize() {
        js = new Joystick();

        entityArray = new Entity[512];
        entCount = 0;

        for (int i = 0; i < 512; i++) {
            entityArray[i] = null;
        }

        projectileArray = new Projectile[512];
        proCount = 0;

        for (int i = 0; i < 512; i++) {
            projectileArray[i] = null;
        }
        
        particleArray = new BaseParticle[512];
        particleCount = 0;
        
        for (int i=0; i < 512; i++)
        {
        	particleArray[i] = null;
        }

        paused = false;
        level = null;

        try
        {
            currentMode = (Gamemode)Config.defaultGamemode.newInstance();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        if (currentMode != null){ currentMode.initialize(); }
        changeGamemodeRequest = null;
    }

    public void onTick() {
    	
    	//change gamemode if want to change
        if (changeGamemodeRequest != null) {
            if (currentMode != null) {
                currentMode.onDie();
            }

            clearEntities();
            js.clearButtons();
            if (Config.useSceneGraph)
                Api.getScene().clearNodes();

            currentMode = changeGamemodeRequest;
            changeGamemodeRequest = null;

            currentMode.initialize();

            paused = false;
        }

        Api.getCamera().onTick();

        if (paused) {
            return;
        }

        //update gamemode
        if (currentMode != null) {
            currentMode.onTick();
        }

        //update joystick + buttons
        if (Api.joystickIsShowing())
            js.onTick();

        
        //update entities
        for (int i = 0; i < entCount; ++i) {
            Entity e = entityArray[i];

            if (e.dead) {
                e.onDie();
                if (i == entCount) {
                    --entCount;
                } else {
                    entityArray[i] = entityArray[entCount - 1];
                    entityArray[entCount - 1] = null;
                    --entCount;
                }
                --i;
                continue;
            }

            e.onTick();
            if (Config.useSceneGraph)
                Api.getScene().onTick();

            if (!e.canCollide()) {
                continue;
            }
            if (e.width == 0 || e.height == 0) {
                continue;
            }

            //entity/entity collision
            for (int j = i; j < entCount; j++) {
                Entity e2 = entityArray[j];

                if (e2.dead) {
                    continue;
                }
                if (!e2.canCollide() || e2.width == 0 || e2.height == 0) {
                    continue;
                }
                if (!e.canCollideWithSelf() && (e.getClass().equals(e2.getClass()))) {
                    continue;
                }

                if (e.x + e.width / 2 < e2.x - e2.width / 2) {
                    continue;
                }
                if (e.y < e2.y - e2.height) {
                    continue;
                }
                if (e.x - e.width / 2 > e2.x + e2.width / 2) {
                    continue;
                }
                if (e.y - e.height > e2.y) {
                    continue;
                }

                e.onCollide(e2);
                e2.onCollide(e);
            }
        }

        //update projectiles
        for (int i = 0; i < proCount; i++) {
            Projectile p = projectileArray[i];

            if (p.dead) {
                p.onDie();
                if (i == proCount) {
                    --proCount;
                } else {
                    projectileArray[i] = projectileArray[proCount - 1];
                    projectileArray[proCount - 1] = null;
                    --proCount;
                } //set last to this
                --i;
                continue;
            }

            p.onTick();

            //entity/projectile collisions
            for (int j = 0; j < entCount; j++) {
                Entity e = entityArray[j];

                if (e.dead) {
                    continue;
                }
                if (!e.canCollide() || e.width == 0 || e.height == 0) {
                    continue;
                }
                if ((p.owner != null) && (p.owner == e)) {
                    continue;
                }
                if (!p.canCollide()) {
                    continue;
                }

                if (p.x + p.width / 2 < e.x - e.width / 2) {
                    continue;
                }
                if (p.y + p.height / 2 < e.y - e.height) {
                    continue;
                }
                if (p.x - p.width / 2 > e.x + e.width / 2) {
                    continue;
                }
                if (p.y - p.height / 2 > e.y) {
                    continue;
                }

                p.onCollide(e);
                e.onCollide(p);
            }
            
           //entity/level collisions
           if (level.isLoaded())
           {
        	   if (p.x < 0){ continue; }
        	   if (p.y < 0){ continue; }
        	   if (p.x >= level.getWidth()*64){ continue; }
        	   if (p.y >= level.getHeight()*64){ continue; }
        	   
        	   if (level.getCellData(p.x/64, p.y/64) != 0)
        	   {
        		   p.dead = true;
        	   }
           }
        }
        
        for (int i=0;i < particleCount; i++)
        {
        	if (particleArray[i].dead)
        	{
        		//Api.getDebug().out("Killing particle");
	            if (i == particleCount) {
	                --particleCount;
	            } else {
	                particleArray[i] = particleArray[particleCount - 1];
	                particleArray[particleCount - 1] = null;
	                --particleCount;
	            } //set last to this
	            --i;
	            continue;
        	}
        	
        	particleArray[i].onTick();
        }
    }

    public void onDraw(GraphicsSubsystem g) {
        if (currentMode != null) {
            currentMode.preDraw(g);
        }
        
        if (level != null && level.isLoaded())
        {
        	int levelX = (Api.getCamera().getX() / 64) - 1;
        	if (levelX < 0){ levelX = 0; }
        	if (levelX > level.getWidth()){ levelX = level.getWidth(); }
        	
        	int levelY = (Api.getCamera().getY() / 64) - 1;
        	if (levelY < 0){ levelY = 0; }
        	if (levelY > level.getHeight()){ levelY = level.getHeight(); }

        	int renderwidth = (g.getScreenWidth() / 64) + 2;
        	int renderheight = (g.getScreenHeight() / 64) + 2;
        	
        	for (int x = levelX;x < BananaMath.min(x+renderwidth,level.getWidth());x++)
        	{
        		for (int y = levelY; y < BananaMath.min(y+renderheight,level.getHeight()); y++)
        		{
        			//Api.getDebug().out(x + "," + y);
        			
        			char cell = level.getCellData(x, y);
        			if (cell == 0){ continue; }
        			
        			int renderx = (x * 64) - Api.getCamera().getX();
        			int rendery = (y * 64) - Api.getCamera().getY();
        			
        			g.setColor(90,90,90);
        			g.drawRectangle( renderx, rendery, 64, 64 );
        			
        			g.setColor(61,61,61);
        			
        			if (x % 2 == 1)
        			{
        				g.drawRectangle( renderx, rendery, 8, 64);
        			}
        			else
        			{
        				g.drawRectangle( renderx + 56, rendery, 8, 64);
        			}
        			
        			if (y % 2 == 1)
        			{
        				g.drawRectangle( renderx, rendery, 64, 8);
        			}
        			else
        			{
        				g.drawRectangle( renderx, rendery + 56, 64, 8);
        			}
        		}
        	}
        }

        if (Config.drawParticlesBeforeEntities)
        {
            for (int i=0;i < particleCount; ++i)
            {
                particleArray[i].onDraw(g);
            }

            for (int i = 0; i < entCount; ++i) {
                entityArray[i].onDraw(g);
            }
        }
        else
        {
            for (int i = 0; i < entCount; ++i) {
                entityArray[i].onDraw(g);
            }

            for (int i=0;i < particleCount; ++i)
            {
                particleArray[i].onDraw(g);
            }
        }

        if (Config.useSceneGraph)
            Api.getScene().onDraw(g);

        for (int i = 0; i < proCount; ++i) {
            projectileArray[i].onDraw(g);
        }
        
        if (currentMode != null) {
            currentMode.onDraw(g);
        }
        if (Api.joystickIsShowing()) {
            js.onDraw(g);
        }
    }

    public boolean onTouch(MotionEvent event) {
        boolean joyTouched = false;
        if (Api.joystickIsShowing()) {
            joyTouched = js.onTouch(event);
        }

        if (joyTouched) {
            return true;
        }
        if (currentMode == null) {
            return false;
        }

        currentMode.onTouch(event);
        return true;
    }

    public void addEntity(Entity e) {
        if (entCount > 510) {
            return;
        }

        e.initialize();
        entityArray[entCount++] = e;
    }

    public void addProjectile(Projectile p) {
        if (proCount > 510) {
            return;
        }

        p.initialize();
        projectileArray[proCount++] = p;
    }
    
    public void addParticle( BaseParticle p )
    {
    	if (particleCount > 510)
    		return;
    	
    	particleArray[particleCount++] = p;
    }

    public void clearEntities() {
        for (int i = 0; i < entCount; i++) {
            entityArray[i] = null;
        }

        entCount = 0;

        for (int i = 0; i < proCount; i++) {
            projectileArray[i] = null;
        }

        proCount = 0;
        
        for (int i=0;i < particleCount; i++)
        {
        	particleArray[i] = null;
        }
        
        particleCount = 0;
    }

    public void changeGamemode(Gamemode newGamemode) {
        changeGamemodeRequest = newGamemode;
    }

    public Gamemode getGamemode() {
        return currentMode;
    }

    public Joystick getJoystick() {
        return js;
    }

    public void togglePause() {
        if (currentMode == null || !currentMode.canPause()) {
            Toast.makeText(Api.getMainActivity(), "Can't pause now.", Toast.LENGTH_SHORT).show();
            return;
        }
        paused = !paused;

        String text = "";
        if (paused) {
            text = "Paused.";
        } else {
            text = "Unpaused.";
        }

        Toast.makeText(Api.getMainActivity(), text, Toast.LENGTH_SHORT).show();
    }
}