package com.nicknytko.android.banana;

import android.graphics.Bitmap;

public class BananaBitmap {
    private Bitmap imgData;
    private int width, height;
    private boolean initialized;

    public int id;

    public BananaBitmap(GraphicsSubsystem gs, Bitmap imgData) {
        id = 0;
        initialize(gs, imgData);
    }

    public BananaBitmap() {
        initialized = false;
        id = 0;
    }

    public void initialize(GraphicsSubsystem gs, Bitmap imgData) {
        this.imgData = imgData;

        width = gs.pixelToDp(imgData.getWidth());
        height = gs.pixelToDp(imgData.getHeight());

        initialized = true;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
    
    public int getActualWidth()
    {
    	return imgData.getWidth();
    }
    
    public int getActualHeight()
    {
    	return imgData.getHeight();
    }

    public Bitmap getImgData() {
        return imgData;
    }
}