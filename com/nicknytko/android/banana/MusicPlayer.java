package com.nicknytko.android.banana;

import android.media.AsyncPlayer;
import android.media.AudioManager;
import android.net.Uri;

public class MusicPlayer
{
    private AsyncPlayer player;

    public MusicPlayer()
    {
        player = new AsyncPlayer("[MusicPlayer]");
        Api.getDebug().out(Api.getPackageName());
    }

    public void play( int rawid, boolean looping )
    {
        String uri = "android.resource://" + Api.getPackageName() + "/" + rawid;
        Api.getDebug().out("Playing " + uri);

        player.play(Api.getMainActivity(), Uri.parse(uri), looping, AudioManager.STREAM_MUSIC );
    }

    public void stop()
    {
        player.stop();
    }
}