package com.nicknytko.android.banana;

import android.content.Context;
import android.os.Vibrator;

public class VibratorSubsystem
{
    private Vibrator vib;
    private final long minVibration = 50;

    public VibratorSubsystem()
    {
        vib = (Vibrator)Api.getMainActivity().getSystemService(Context.VIBRATOR_SERVICE);
    }

    public boolean hasVibrator()
    {
        return vib.hasVibrator();
    }

    public void vibrate( long millis )
    {
        if (millis < minVibration)
            millis = minVibration;

        vib.vibrate( millis );
    }
}