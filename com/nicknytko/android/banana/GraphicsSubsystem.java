package com.nicknytko.android.banana;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.graphics.Canvas;
import android.graphics.drawable.NinePatchDrawable;

public class GraphicsSubsystem {
    private float density;
    private int densityInt;

    private int screenWidth;
    private int screenHeight;

    private int screenActualWidth;
    private int screenActualHeight;

    private Bitmap.Config bitmapConfig;
    private GraphicsLoader loader;

    private Canvas drawing;
    private Paint drawingPaint;
    private Paint fontPaint;

    public int pixelToDp(int px) {
        if (densityInt == 200) {
            return px / 2;
        }
        if (densityInt == 100) {
            return px;
        }
        return (px * 100) / densityInt;
    }

    public int dpToPixel(int dp) {
        //return dp;
        //if (densityInt == 200){ return dp*2; }
        //if (densityInt == 100){ return dp; }
        return (dp * densityInt) / 100;
    }

    public GraphicsSubsystem() {
        DisplayMetrics dm = Api.getMainActivity().getResources().getDisplayMetrics();
        density = dm.density;
        densityInt = (int) (density * 100.0f);

        Api.getDebug().out("Screen density is " + density + ".");

        screenActualWidth = dm.widthPixels;
        screenActualHeight = dm.heightPixels;

        Api.getDebug().out("Screen is " + screenActualWidth + "x" + screenActualHeight + " pixels.");

        screenWidth = pixelToDp(screenActualWidth);
        screenHeight = pixelToDp(screenActualHeight);

        Api.getDebug().out("Screen is " + screenWidth + "x" + screenHeight + " dp.");

        if (screenWidth > 640 && screenHeight > 480)
        {
        	density *= 1.5f;
        	densityInt = (int)(density * 100.0f);
        	
            screenWidth = pixelToDp(screenActualWidth);
            screenHeight = pixelToDp(screenActualHeight);
        }
        
        bitmapConfig = Bitmap.Config.ARGB_8888;
        loader = new GraphicsLoader(this);

        drawing = null;
        drawingPaint = new Paint();
        drawingPaint.setARGB(255, 255, 255, 255);
        fontPaint = new Paint();
        fontPaint.setARGB(255, 0, 0, 0);
    }

    public float getDensity() {
        return density;
    }

    public int getScreenWidth() {
        return screenWidth;
    }

    public int getScreenHeight() {
        return screenHeight;
    }

    public int getHalfWidth() { return screenWidth/2; }

    public int getHalfHeight() { return screenHeight/2; }

    public int getActualScreenWidth() {
        return screenActualWidth;
    }

    public int getActualScreenHeight() {
        return screenActualHeight;
    }

    public Bitmap.Config getBitmapConfig() {
        return bitmapConfig;
    }

    public void setCanvas(Canvas drawing) {
        this.drawing = drawing;
    }

    public Canvas getCanvas() {
        return drawing;
    }

    public BananaBitmap loadImage(int id) {
        return loader.getImage(id);
    }

    public void setColor(int r, int g, int b) {
        drawingPaint.setARGB(drawingPaint.getAlpha(), r, g, b);
    }

    public void setColor(int r, int g, int b, int a) {
        drawingPaint.setARGB(a, r, g, b);
    }

    public void setAlpha(int a) {
        drawingPaint.setAlpha(a);
    }

    public void setFontColor(int r, int g, int b) {
        fontPaint.setARGB(255, r, g, b);
    }

    public void setFontSize(int size) {
        fontPaint.setTextSize(dpToPixel(size));
    }

    public void setFontStyle(Paint.Style style) {
        fontPaint.setStyle(style);
    }

    public void setFontStrokeWidth(int size) {
        fontPaint.setStrokeWidth(dpToPixel(size));
    }

    public void setFontFace(Typeface f) {
        fontPaint.setTypeface(f);
    }

    public void setDefaultFontFace() {
        fontPaint.setTypeface(null);
    }

    public void setDrawStroke()
    {
        drawingPaint.setStyle( Paint.Style.STROKE );
    }

    public void setDrawFill()
    {
        drawingPaint.setStyle( Paint.Style.FILL );
    }

    public void setDrawStrokeWidth( float w)
    {
        drawingPaint.setStrokeWidth( w );
    }

    public void setFontPaint(Paint p) {
        fontPaint = p;
    }

    public Paint getFontPaint() {
        return fontPaint;
    }

    public void drawBitmap(BananaBitmap bmp, int x, int y) {
        drawing.drawBitmap(bmp.getImgData(), dpToPixel(x), dpToPixel(y), null);
    }

    public void drawScaledBitmap(BananaBitmap bmp, int x, int y, float scalex, float scaley )
    {
        x = dpToPixel(x);
        y = dpToPixel(y);

        Matrix m = new Matrix();
        m.postScale(scalex,scaley);
        m.postTranslate( x, y );

        drawing.drawBitmap( bmp.getImgData(), m, null );
    }

    public void drawRotatedBitmap(BananaBitmap bmp, int x, int y, float degrees, int pivotX, int pivotY) {
        x = dpToPixel(x);
        y = dpToPixel(y);
        pivotX = dpToPixel(pivotX);
        pivotY = dpToPixel(pivotY);

        Matrix m = new Matrix();
        m.postRotate(degrees, pivotX, pivotY);
        m.postTranslate(x, y);

        drawing.drawBitmap(bmp.getImgData(), m, null);
    }

    public void drawNinePatch(NinePatchDrawable patch, int x, int y, int width, int height) {
        Rect r = new Rect(dpToPixel(x), dpToPixel(y), dpToPixel(x + width), dpToPixel(y + height));
        patch.setBounds(r);
        patch.draw(drawing);
    }

    public void drawLine( int x1, int y1, int x2, int y2 )
    {
        int actualX1 = dpToPixel(x1);
        int actualY1 = dpToPixel(y1);
        int actualX2 = dpToPixel(x2);
        int actualY2 = dpToPixel(y2);

        drawing.drawLine(actualX1,actualY1,actualX2,actualY2,drawingPaint);
    }

    public void drawRectangle(int x1, int y1, int width, int height) {
        int actualX = dpToPixel(x1);
        int actualY = dpToPixel(y1);

        drawing.drawRect(actualX, actualY, actualX + dpToPixel(width), actualY + dpToPixel(height), drawingPaint);
    }

    public void drawCircle( int x, int y, int radius ){
        int actualX = dpToPixel(x);
        int actualY = dpToPixel(y);

        drawing.drawCircle( actualX, actualY, dpToPixel(radius), drawingPaint);
    }

    public void drawArc( int x, int y, int radius, float startAngle, float sweepAngle )
    {
        RectF temp = new RectF();
        temp.left = dpToPixel(x) - dpToPixel(radius);
        temp.top = dpToPixel(y) - dpToPixel(radius);
        temp.right = dpToPixel(x) + dpToPixel(radius);
        temp.bottom = dpToPixel(y) + dpToPixel(radius);

        drawing.drawArc(temp, startAngle, sweepAngle, false, drawingPaint );
    }

    public void drawTriangle( int x1, int y1, int x2, int y2, int x3, int y3 )
    {
        int actualX1 = dpToPixel(x1);
        int actualY1 = dpToPixel(y1);
        int actualX2 = dpToPixel(x2);
        int actualY2 = dpToPixel(y2);
        int actualX3 = dpToPixel(x3);
        int actualY3 = dpToPixel(y3);

        Path path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);
        path.moveTo(actualX1, actualY1);
        path.lineTo(actualX2, actualY2);
        path.lineTo(actualX3, actualY3);
        path.lineTo(actualX1, actualY1);
        path.close();

        drawing.drawPath(path, drawingPaint);
    }

    public void drawText(int x, int y, String str) {
        fontPaint.setTextAlign(Paint.Align.LEFT);
        drawing.drawText(str, dpToPixel(x), dpToPixel(y), fontPaint);
    }

    public void drawCenterText(int x, int y, String str) {
        fontPaint.setTextAlign(Paint.Align.CENTER);
        drawing.drawText(str, dpToPixel(x), dpToPixel(y), fontPaint);
    }

    public void drawRightText(int x, int y, String str) {
        fontPaint.setTextAlign(Paint.Align.RIGHT);
        drawing.drawText(str, dpToPixel(x), dpToPixel(y), fontPaint);
    }

    public void drawCircularText( int x, int y, int radius, float startAngle, float sweepAngle, String str )
    {
        RectF temp = new RectF();
        temp.left = dpToPixel(x) - dpToPixel(radius);
        temp.top = dpToPixel(y) - dpToPixel(radius);
        temp.right = dpToPixel(x) + dpToPixel(radius);
        temp.bottom = dpToPixel(y) + dpToPixel(radius);

        Path path = new Path();
        //path.addCircle( dpToPixel(x), dpToPixel(y), dpToPixel(radius), Path.Direction.CW );
        path.addArc(temp, startAngle, sweepAngle);

        fontPaint.setTextAlign(Paint.Align.CENTER);
        drawing.drawTextOnPath(str, path, 0, 0, fontPaint );
    }

    public void clearScreen() {
        drawing.drawARGB(255, 0, 0, 0);
    }

    public void clearScreen(int r, int g, int b) {
        drawing.drawARGB(255, r, g, b);
    }
}