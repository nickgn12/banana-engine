package com.nicknytko.android.banana;

import android.view.MotionEvent;

public class Gamemode 
{
    public void initialize()
    {
    }

    public void preDraw(GraphicsSubsystem g)
    {
    }

    public void onDraw(GraphicsSubsystem g)
    {
    }

    public void onTick()
    {
    }

    public boolean onTouch(MotionEvent event)
    {
        return true;
    }

    public void onDie()
    {
    }

    public void onBackButton()
    {
    }

    public boolean canPause()
    {
        return false;
    }
}