package com.nicknytko.android.banana;

public class JoystickButton {
    public int x, y;
    protected BananaBitmap image;

	protected float pressure;
    
    public boolean isPressed;
    public boolean isDown;
    public boolean isReleased;

    public int pointerId;

    public JoystickButton(int x, int y, int imageid) {
        this.x = x;
        this.y = y;
        
        if (imageid != 0)
        	image = Api.getGraphicsSubsystem().loadImage(imageid);

        isPressed = false;
        isDown = false;
        isReleased = false;

        pointerId = -1;
        pressure = 0;
    }
    
    public float getWidth()
    {
    	return image.getWidth();
    }
    
    public float getHeight()
    {
    	return image.getHeight();
    }

    public float getDiameter() {
        return (int) BananaMath.max(getWidth(), getHeight());
    }

    public float getRadius() {
        return getDiameter() / 2;
    }

    public BananaBitmap getBitmap() {
        return image;
    }
    
    public void setPressure( float p ){ pressure = p; }
    public float getPressure(){ return pressure; }

    public boolean isPressed(int touchx, int touchy, float touchRadius) {
        float dist = BananaMath.getDistance(touchx, touchy, x, y );

        return (dist <= touchRadius + getRadius());
    }
    
    public void onDraw( GraphicsSubsystem g )
    {
    	g.drawBitmap(image, x - image.getWidth()/2, y - image.getHeight()/2);
    }
}