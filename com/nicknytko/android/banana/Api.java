package com.nicknytko.android.banana;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;

import com.nicknytko.android.banana.scene.Scene;

public class Api {
    private static long ticks;
    private static boolean showingJoystick;

    private static Accelerometer accelerometer;
    private static BananaNet net;
    private static BananaView bview;
    private static Camera camera;
    private static Debug debug;
    private static FileIO fileIo;
    private static Game curGame;
    private static GlobalVarTable globalVars;
    private static GraphicsSubsystem graphics;
    private static Activity bactivity;
    private static MusicPlayer music;
    private static MainThread mthread;
    private static Scene scene;
    private static SoundSubsystem sound;

    private static VibratorSubsystem vibrator;

    public static Activity getMainActivity() {
        return bactivity;
    }

    public static BananaView getMainView() {
        return bview;
    }

    public static MainThread getMainThread() {
        return mthread;
    }

    public static GraphicsSubsystem getGraphicsSubsystem() {
        return graphics;
    }
    
    public static GraphicsSubsystem getGraphics()
    {
    	return graphics;
    }
    
    public static SoundSubsystem getSoundSubsystem()
    {
    	return sound;
    }
    
    public static SoundSubsystem getSound()
    {
    	return sound;
    }

    public static Game getGame() {
        return curGame;
    }

    public static Debug getDebug() {
        return debug;
    }

    public static FileIO getFileIO() {
        return fileIo;
    }

    public static Accelerometer getAccelerometer() { return accelerometer; }

    public static VibratorSubsystem getVibrator() { return vibrator; }

    public static boolean joystickIsShowing() {
        return showingJoystick;
    }

    public static GlobalVarTable getGlobalVars() {
        return globalVars;
    }

    public static Joystick getJoystick() {
        return curGame.getJoystick();
    }

    public static Camera getCamera() {
        return camera;
    }

    public static long getTicks() {
        return ticks;
    }

    public static BananaNet getNet() {
        return net;
    }

    public static Scene getScene() {
        return scene;
    }

    public static MusicPlayer getMusicPlayer(){ return music; }

    public static void registerMainActivity(Activity bactivity) {
        Api.bactivity = bactivity;
    }

    public static void registerMainView(BananaView bview) {
        Api.bview = bview;
    }

    public static void registerMainThread(MainThread mthread) {
        Api.mthread = mthread;
    }

    public static void registerGraphicsSubsystem(GraphicsSubsystem graphics) {
        Api.graphics = graphics;
    }

    public static void registerGame(Game curGame) {
        Api.curGame = curGame;
    }

    public static void showJoystick(boolean show) {
        showingJoystick = show;
    }

    public static void incrementTicks() {
        ticks++;
    }

    public static void simulateBackButton() {
        curGame.handleBackButton();
    }

    public static void simulateButtonPress(int keyCode) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                simulateBackButton();
                break;
            case KeyEvent.KEYCODE_MENU:
                Api.getGame().togglePause();
                break;
            default:
                break;
        }
    }

    public static String getPackageName()
    {
        return Config.appPackageName;
    }

    public static void initEngine() {
        if (bactivity == null) {
            return;
        }
        bview = new BananaView(bactivity);

        debug = new Debug();
        fileIo = new FileIO();
        globalVars = new GlobalVarTable();
        camera = new Camera();

        if (Config.useNetwork)
            net = new BananaNet();

        if (Config.useSceneGraph)
            scene = new Scene();

        if (Config.useSound)
            sound = new SoundSubsystem();

        if (Config.useMusic)
            music = new MusicPlayer();

        if (Config.useAccelerometer)
            accelerometer = new Accelerometer();

        if (Config.useVibrator)
            vibrator = new VibratorSubsystem();

        showingJoystick = false;
        ticks = 0;

        bactivity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        bactivity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        bactivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        bactivity.setContentView(bview);
    }

    public static void quit() {
        bactivity.finish();
    }
}