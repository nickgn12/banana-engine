package com.nicknytko.android.banana;

import java.util.Vector;

import android.view.MotionEvent;

public class Joystick {
    private int joystickDragging;
    private float joyx, joyy;

    public BananaBitmap imgJoystick;
    public BananaBitmap imgJoyTop;

	@SuppressWarnings("unused")
	private int scrWidth, scrHeight;
    private Vector<JoystickButton> buttons;

    public boolean buttonsOnly;

    public float getJoyX() {
        return joyx / 50.0f;
    }

    public float getJoyY() {
        return joyy / 50.0f;
    }

    public void clearButtons() {
        buttons.clear();
    }

    public void addButton(JoystickButton b) {
        buttons.add(b);
    }

    public Joystick() {
        imgJoystick = Api.getGraphicsSubsystem().loadImage(Config.joystickImage);
        imgJoyTop = Api.getGraphicsSubsystem().loadImage(Config.joystickTopImage);

        scrWidth = Api.getGraphicsSubsystem().getScreenWidth();
        scrHeight = Api.getGraphicsSubsystem().getScreenHeight();

        joystickDragging = -1;
        joyx = joyy = 0;

        buttons = new Vector<JoystickButton>();
        buttonsOnly = false;
    }

    public void onTick() {
        for (int i = 0; i < buttons.size(); i++) {
            buttons.get(i).isPressed = false;
            
            if (buttons.get(i).isReleased == true)
            {
            	buttons.get(i).setPressure(0);
            	buttons.get(i).isDown = false;
                buttons.get(i).isReleased = false;
            }
        }
    }

    public void onDraw(GraphicsSubsystem g) {
        if (!buttonsOnly)
        {
            g.drawBitmap(imgJoystick, 40, g.getScreenHeight() - 30 - imgJoystick.getHeight());
            g.drawBitmap(imgJoyTop, 40 + (int) (imgJoystick.getWidth() / 2 - imgJoyTop.getWidth() / 2) + (int) (joyx * 10), g.getScreenHeight() - 30 - imgJoystick.getHeight() + (int) (imgJoystick.getHeight() / 2 - imgJoyTop.getHeight() / 2) + (int) (joyy * 10));
        }

        for (int i = 0; i < buttons.size(); i++)
        {
            //g.drawBitmap(buttons.get(i).getBitmap(), buttons.get(i).x, buttons.get(i).y);
        	buttons.get(i).onDraw( g );
        }
    }

    public boolean onTouch( MotionEvent event )
    {
    	int actionIndex = event.getActionIndex();
        int actionCode = event.getActionMasked();
        
        int actionX = Api.getGraphicsSubsystem().pixelToDp((int) event.getX(actionIndex));
        int actionY = Api.getGraphicsSubsystem().pixelToDp((int) event.getY(actionIndex));

        boolean isPressed = false;
        
        if ((actionCode == MotionEvent.ACTION_UP || actionCode == MotionEvent.ACTION_POINTER_UP)) 
        {
        	if (!buttonsOnly && actionIndex == joystickDragging)
        	{
	            joystickDragging = -1;
	            joyx = joyy = 0;
	            isPressed = true;
        	}

            for (int i = 0; i < buttons.size(); i++) {
                if (buttons.get(i).pointerId == actionIndex || actionCode==MotionEvent.ACTION_UP) 
                {
                    buttons.get(i).pointerId = -1;
                    buttons.get(i).isDown = false;
                    buttons.get(i).isReleased = true;
                    buttons.get(i).setPressure(0);
                }
            }
            isPressed = true;
        }
        if (actionCode == MotionEvent.ACTION_UP)
        {
        	joystickDragging = -1;
        	joyx = joyy = 0;
        	isPressed = true;
        }
        
        if (!buttonsOnly && joystickDragging != -1)
        {
        	int index = event.findPointerIndex(joystickDragging);
        	
        	if (index < event.getPointerCount())
        	{
	            float movex = Api.getGraphicsSubsystem().pixelToDp((int)event.getX(index)) - (40 + imgJoystick.getWidth() / 2);
	            float movey = Api.getGraphicsSubsystem().pixelToDp((int)event.getY(index)) - (scrHeight - 30 - imgJoystick.getHeight() / 2);
	            
	            if (movex < -50) {
	                movex = -50;
	            }
	            if (movex > 50) {
	                movex = 50;
	            }
	
	            if (movey < -50) {
	                movey = -50;
	            }
	            if (movey > 50) {
	                movey = 50;
	            }
	
	            joyx = movex / 10;
	            joyy = movey / 10;
	            
	            isPressed = true;
        	}
        }

        if (!buttonsOnly && event.getPointerCount() < 1 && joystickDragging != -1)
        {
        	joystickDragging = -1;
        	joyx = 0;
        	joyy = 0;
            for (int i = 0; i < buttons.size(); i++) 
            {
                if (buttons.get(i).pointerId == actionIndex || actionCode==MotionEvent.ACTION_UP) 
                {
                    buttons.get(i).pointerId = -1;
                    buttons.get(i).isDown = false;
                    buttons.get(i).isReleased = true;
                    buttons.get(i).setPressure(0);
                }
            }
        }
        
        if (actionCode == MotionEvent.ACTION_DOWN || actionCode == MotionEvent.ACTION_POINTER_DOWN) 
        {
            for (int i = 0; i < buttons.size(); i++) 
            {
                if (buttons.get(i).isPressed(actionX, actionY, event.getSize(actionIndex))) 
                {
                    buttons.get(i).isPressed = true;
                    buttons.get(i).isDown = true;
                    buttons.get(i).pointerId = actionIndex;
                    buttons.get(i).setPressure(event.getPressure(actionIndex));
                    isPressed = true;
                }
            }
            
            if (!buttonsOnly && BananaMath.getDistance(90, (scrHeight - 30 - imgJoystick.getHeight() + 50), actionX, actionY) <= imgJoystick.getWidth()) {
                joystickDragging = actionIndex;
                isPressed = true;
            }
        }
        
        return isPressed;
    }
}