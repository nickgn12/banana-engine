package com.nicknytko.android.banana;

public class Level
{
	private int width, height;
	char[] levelData;
	
	private boolean loaded;
	
	public boolean isLoaded(){ return loaded; }
	public int getWidth(){ return width; }
	public int getHeight(){ return height; }
	
	public Level( int width, int height )
	{
		this.width = width;
		this.height = height;
		
		levelData = new char[width*height];
		
		loaded = false;
	}
	
	public void loadFromString( String data )
	{
		if (data.length() != width*height)
		{
			Api.getDebug().out("String is not the same size as level data!");
			Api.getDebug().out("Got " + data.length() + ". Expected " + (width*height) + ".");
			return;
		}
		
		for (int i=0;i < data.length();i++)
		{
			char c = data.charAt(i);
			
			if (c == '0'){ levelData[i] = 0; }
			if (c >= 65){ levelData[i] = (char) (c - 64); }
		}
		
		Api.getDebug().out("Successfully loaded level from string!");
		loaded = true;
	}
	
	public int getCell( int x, int y )
	{
		return (y * width) + x;
	}
	
	public char getCellData( int x, int y )
	{
		return levelData[getCell(x,y)];
	}
}